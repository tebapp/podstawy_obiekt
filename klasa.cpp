#include <iostream>
#include <fstream>

using namespace std;

typedef unsigned int uint;
typedef unsigned char uchar;

class Osoba {
private:
    string *imie,*nazwisko;
    uchar *plec;
    uint *id_miasto;
    string *ulica;
    string *nr_domu,*nr_mieszkania;
    string *data_urodzenia;
    string *pesel;

    uint wskaznik, max_element;
public:
    Osoba() : Osoba(100) { }

    Osoba(int max_element) {
        this->imie = new string[max_element];
        this->nazwisko = new string[max_element];
        plec = new uchar[max_element];
        ulica = new string[max_element];
        nr_domu = new string[max_element];
        nr_mieszkania = new string[max_element];
        data_urodzenia = new string[max_element];
        pesel = new string[max_element];
        wskaznik = 0;
        this->max_element = max_element;
    }

    ~Osoba() {
        delete [] imie;
        delete [] nazwisko;
        delete [] pesel;
        //trzeba tak niszczyc WSZYSTKIE ZMIENNE WSKAZNIKOWE!
        cout << "NISZCZE OBIEKT!";
    }

    string getImie() {
        if (wskaznik>=max_element)
            return "Nie ma takiego indeksu!";
        else if (imie[wskaznik].empty())
            return "Nie ma imienia pod tym indeksem!";
        return imie[wskaznik];
    }

    string getNazwisko() {
        if (wskaznik>=max_element)
            return "Nie ma takiego indeksu!";
        else if (nazwisko[wskaznik].empty())
            return "Nie ma nazwisko pod tym indeksem!";
        return nazwisko[wskaznik];
    }

    string getPesel() {
        if (wskaznik>=max_element)
            return "Nie ma takiego indeksu!";
        else if (pesel[wskaznik].empty())
            return "Nie ma PESLA pod tym indeksem!";
        return pesel[wskaznik];
    }

    bool wczytaj(string nazwa="osoby.txt") {
        ifstream plik(nazwa);
        wskaznik=0;
        if (!plik.is_open()) {
            cerr << "Blad otwarcia pliku (brak pliku lub uprawnien)" << endl;
            return false;
        }
        while (!plik.eof()) {
            plik >> imie[wskaznik] >>
                    nazwisko[wskaznik] >>
                    pesel[wskaznik];
            wskaznik++;
        }
        plik.close();
        return true;
    }

    bool zapisz(string nazwa="osoby.txt") {
        ofstream plik(nazwa);
        if (!plik.is_open()) {
            cerr << "Blad otwarcia pliku (brak pliku lub uprawnien)" << endl;
            return false;
        }
        for (uint i =0;i<max_element;i++)
            plik << imie[i] << ' ' << nazwisko[i] << ' ' << pesel[i] << '\n';
        plik.close();
    }

    bool usun() {
        for (int i=wskaznik+1;i<max_element;i++) {
            imie[i-1]=imie[i];
            nazwisko[i-1]=nazwisko[i];
            pesel[i-1]=pesel[i];
        }
        if (wskaznik<max_element-1) {
            imie[max_element-1]="";
            nazwisko[max_element-1]="";
            pesel[max_element-1]="";
            return true;
        }
        return false;
    }

    void wypisz(uint i=0) {
        for(;i<max_element;i++) {
            if (imie[i].empty() && nazwisko[i].empty() &&
                    pesel[i].empty()) continue;
            cout << imie[i] << ' ' << nazwisko[i]
                    << ' ' << pesel[i] << endl;
        }
    }

    void setWskaznik(uint w) {this->wskaznik=w;}
};

//struct Osoba {
//    string imie,nazwisko;
//    //bool kobieta;
//    uchar plec;
//    uint id_miasto;
//    string ulica;
//    string nr_domu,nr_mieszkania;
//    string data_urodzenia;
//    string pesel;
//};

struct Miasto {
    string miasto,kod,opis;
};


///należy zaimplementować
void odczytaj_miasta(string nazwa, Miasto *m);
void zapisz_miasta(string nazwa, Miasto *m, int max);
void odczytaj_osoby(string nazwa, Osoba *o);
void zapisz_osoby(string nazwa, Osoba *o, int max);
Miasto pobierz_miasto(int id, Miasto *m);
Osoba pobierz_osobe(int id, Osoba *o);
bool usun_miasto(int id, Miasto *m, int max);
bool usun_osobe(int id, Osoba *o, int max);
bool zamien_miasto(Miasto m, int id, Miasto *mp);
bool zamien_osobe(Osoba o, int id, Osoba *op);

//przetestowac na 20 wpisach!

int menu();
string get_nazwa() {string r; cout << "\nPodaj nazwe pliku: "; cin >> r; return r;}

int main()
{

    int max_miasta=30;

    Miasto miasta[max_miasta];
    Osoba osoby;
    while (1) {
        switch (menu()) {
        case 1: odczytaj_miasta(get_nazwa(), miasta);
            break;
        case 2: osoby.wczytaj(); break;
        case 3: zapisz_miasta(get_nazwa(), miasta, max_miasta);break;
        //case 4: zapisz_osoby(get_nazwa(), osoby, max_osoby);break;
        case 10: osoby.wypisz(); break;
        default:
            return 0;
        }
    }

    return 0;
}

int menu() {
    int opcja = 0;
    cout << "Wybierz jedna z opcji:\n1 - Wczytaj miasta z pliku\n2 - Wszytaj osoby z pliku\n3 - Zapisz miasta do pliku"
         << "\n4 - Zapisz osoby do pliku\n5 - Wyswietl miasta\n6 - Wyswietl osoby\n7 - Usun miasto\n8 - Usun osobe"
         << "\n9 - Zmien miasto\n10 - Zmien osobe\n11 - Koniec programu\nTwoj wybor: ";
    cin >> opcja;
    return opcja;
}

void odczytaj_miasta(string nazwa, Miasto *m) {
    ifstream plik(nazwa);
    int p=0;
    if (!plik.is_open()) return;
    while(!plik.eof()) {
        plik >> m[p].miasto >> m[p].kod /*>> m[p].opis*/;
        p++;
    }
    plik.close();
}

void zapisz_miasta(string nazwa, Miasto *m, int max) {
    ofstream plik(nazwa);
    if (!plik.is_open()) return;
    for (int i =0;i<max;i++)
        plik << m[i].miasto << ' ' << m[i].kod << '\n';
    plik.close();
}

//void odczytaj_osoby(string nazwa, Osoba *o) {
//    ifstream plik(nazwa);
//    int p=0;
//    if (!plik.is_open()) return;
//    while (!plik.eof()) {
//        plik >> o[p].getImie() >> o[p].nazwisko >> o[p].pesel;
//        p++;
//    }
//    plik.close();
//}

//void zapisz_osoby(string nazwa, Osoba *o, int max) {
//    ofstream plik(nazwa);
//    if (!plik.is_open()) return;
//    for (int i =0;i<max;i++)
//        plik << o[i].imie << ' ' << o[i].nazwisko << ' ' << o[i].pesel << '\n';
//    plik.close();
//}

Miasto pobierz_miasto(int id, Miasto *m) {
    return m[id];
}

Osoba pobierz_osobe(int id, Osoba *o) {
    return o[id];
}

bool usun_miasto(int id, Miasto *m, int max) {
    for (int i=id+1;i<max;i++)
        m[i-1]=m[i];
    if (id<max-1) {
        m[max-1].miasto="";
        m[max-1].kod="";
        return true;
    }
    return false;
}

//bool usun_osobe(int id, Osoba *o, int max) {
//    for (int i=id+1;i<max;i++)
//        o[i-1]=o[i];
//    if (id<max-1) {
//        o[max-1].imie="";
//        o[max-1].nazwisko="";
//        o[max-1].pesel="";
//        return true;
//    }
//    return false;
//}

bool zamien_miasto(Miasto m, int id, Miasto *mp) {

    mp[id]=m;
    return true;
}

bool zamien_osobe(Osoba o, int id, Osoba *op) {
    op[id]=o;
    return true;
}
